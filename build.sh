#!/bin/bash

# Build tzscan from command line
#
#  * External dependencies are mirrored
#  * Build 3 verions of binaries for mainnet, alphanet, and zeronet
#  * Hopefully works fine with the existing OCaml installation
#
# Target
#
#  * Ubuntu 18.04
#
# Requirements
#
#  * sudo
#
# How to:
#
#  $ ./build.sh
#
# TODOs
#
#  * build cleanup
#  * server configurations

set -e

WHOAMI=`whoami` # You are the DB owner and who compiles
ARCHIVE=https://gitlab.com/tezos-southeast-asia/opam-repository-for-tzscan/raw/master/archive
TZSCAN_COMMIT=6c960421

echo "========="
echo config
echo "========="

if [ -n "$1" ]; then
    echo "Using $1 instead of ./build.config"
    . $1
else
    . ./build.config
fi

echo "========="
echo apt install
echo "========="

if [ ! -f /usr/bin/sudo ]; then
    # assumes the use by CI, executed by root
    echo installing sudo
    apt update
    apt upgrade -y
    apt install sudo
fi

# sudo apt clean && apt update && apt upgrade -y
sudo su -c 'DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
  curl \
  git \
  ca-certificates \
  make \
  patch \
  sudo \
  g++ \
  pkg-config \
  libgmp-dev \
  libev-dev \
  libhidapi-dev \
  m4 \
  unzip \
  bubblewrap \
  postgresql \
  libsodium-dev \
  libgeoip1 \
  geoip-database \
  libcurl4-gnutls-dev \
  zlib1g-dev \
  libgeoip-dev \
  tzdata \
  rsync'

echo "========"
echo tzscan source
echo "========"

if [ ! -d tzscan/.git ]; then
    echo "Downloading tzscan source code"
    git clone https://gitlab.com/tezos-southeast-asia/tzscan.git
    (cd tzscan; git checkout $TZSCAN_COMMIT)
else
    echo "Skipping tzscan source code download"
fi

echo "========="
echo opam install
echo "========="
export OPAM=`pwd`/opam.exe
if [ -f $OPAM ] ; then
    echo "Skipping installating of opam"
else
    curl -L -O https://github.com/ocaml/opam/releases/download/2.0.1/opam-2.0.1-x86_64-linux
    mv opam-2.0.1-x86_64-linux opam.exe
    chmod +x opam.exe
fi

if [ -d $HOME/.opam ]; then
    echo "Skipping opam init"
else
    $OPAM init -a --bare --disable-sandboxing
fi

if [ -d _opam ]; then
    echo "Skipping opam switch create . 4.07.1"
else
    $OPAM switch create . ocaml-base-compiler.4.07.1
fi

if $OPAM repo | grep opam-repository-for-tzscan; then
    echo "The mirror repo seems to be already added"
else
    echo "Adding the mirror opam repository"
    $OPAM repo add tzscan git+https://gitlab.com/tezos-southeast-asia/opam-repository-for-tzscan
fi
$OPAM repo remove default
if $OPAM repo | grep https://opam\.ocaml\.org; then
    echo "Something went wrong at setting the mirror repo"
    exit 1
fi
   
eval $($OPAM env)

echo "========="
echo installing opam packages
echo "========="

$OPAM install -y \
     "base64" \
     "calendar" \
     "camlp4" \
     "cohttp-lwt" \
     "cohttp-lwt-unix" \
     "csv-lwt" \
     "ezjsonm" \
     "geoip" \
     "js_of_ocaml" \
     "js_of_ocaml-camlp4" \
     "js_of_ocaml-lwt" \
     "js_of_ocaml-ppx" \
     "js_of_ocaml-tyxml" \
     "lwt_log" \
     "nocrypto" \
     "ocamlfind" \
     "ocp-build" \
     "ocplib-endian" \
     "ocurl" \
     "omd" \
     "rresult" \
     "sodium" \
     "zarith"

echo "========="
echo preparing tzscan
echo "========="

cd ./tzscan

DOWNLOAD_DIR=../download

if [ ! -d ${DOWNLOAD_DIR} ]; then
    mkdir ${DOWNLOAD_DIR}
fi

download_and_extract () {
    if [ ! -f ${DOWNLOAD_DIR}/$2.tgz ]; then
	curl -L -o ${DOWNLOAD_DIR}/$2.tgz ${ARCHIVE}/$2.tgz
    else
	echo "${DOWNLOAD_DIR}/$2.tgz is already downloaded"
    fi
    tar xf ${DOWNLOAD_DIR}/$2.tgz -C $1
}

download_and_extract libs pgocaml-7bd9a683

download_and_extract static amcharts3-00193340
download_and_extract static ammap3-8a842e4b

echo "========="
echo installing pgocaml
echo "========="

# OPAM may be initialized with sandboxing.
# In that case, due to its chroot, PGOCaml fails to configure itself:
# the PosgreSQL unix domain socket path is misconfigured.
#
# Therefore, we have to build PGOCaml and tzscan by make, not by OPAM.

# PGOCaml config works incorrectly when sandboxing is on.
# Therefore we install it NOT by opam but by make
ocamlfind remove pgocaml
(cd libs/pgocaml; \
 ./configure --enable-p4 --prefix $OPAM_SWITCH_PREFIX --docdir $OPAM_SWITCH_PREFIX/doc; \
 make; \
 make install)

echo "========="
echo initializing postgresql
echo "========="

sudo /etc/init.d/postgresql start
sudo su postgres -c psql <<EOF
CREATE USER ${WHOAMI} ;
ALTER ROLE ${WHOAMI} CREATEDB;
EOF

echo "========="
echo installing ocplib-json-typed
echo "========="

(cd libs/ocplib-json-typed; $OPAM install -y .)
 
echo "========="
echo installing tzscan
echo "========="

rm -rf out-*

# www/info.json

networks=""
if [ -n "$mainnet_web_address" ]; then
    if [ -n "$networks" ]; then
	networks="$networks, "
    fi
    networks=$networks"[ \"Mainnet\", \"$mainnet_web_address\" ]"
fi
if [ -n "$alphanet_web_address" ]; then
    if [ -n "$networks" ]; then
	networks="$networks, "
    fi
    networks=$networks"[ \"Alphanet\", \"$alphanet_web_address\" ]"
fi
if [ -n "$zeronet_web_address" ]; then
    if [ -n "$networks" ]; then
	networks="$networks, "
    fi
    networks=$networks"[ \"Zeronet\", \"$zeronet_web_address\" ]"
fi

sed -e "s|%%NETWORKS%%|$networks|g" ../jsons/info.json.temp > static/info.json

# config.json

rm config.json
config_entry_temp="`cat ../jsons/config.json.temp`"

# make_config <dbname>
# return config_entry
make_config () {
    DB=$1
    eval "URL=\$${DB}_tezos_node"
    config_entry=""
    if [ -n "$URL" ]; then
	PROTO=`echo $URL | sed -e 's/:.*//'`
	if [ "$PROTO" != "http" ]; then
	    echo "Error: strange protocol for tezos node URL: $DB_tezos_node=$URL  Only http is allowed."
	    exit 1
	fi
	URLSITE=`echo $URL | sed -e 's/:[0-9]*$//'`
	URLPORT=`echo $URL | sed -e 's/^.*://'`
	if [ -z "$URLSITE" -o -z "$URLPORT" ]; then
	    echo "Error: invalid tezos node URL: $DB_tezos_node=$URL"
	    exit 1
        fi
	echo "CONFIG: DB=$DB URLSITE=$URLSITE URLPORT=$URLPORT"
	config_entry="`echo "$config_entry_temp" | sed -e "s|%DB%|$DB|g" -e "s|%URLSITE%|$URLSITE|g" -e "s|%URLPORT%|$URLPORT|g"`"
	echo "config_entry=$config_entry"
    fi		
}

config_json=""
make_config mainnet
if [ -n "$config_entry" ]; then
    if [ -n "$config_json" ]; then
	config_json="$config_json,
"
    fi
    config_json="${config_json}${config_entry}"
fi
make_config alphanet
if [ -n "$config_entry" ]; then
    if [ -n "$config_json" ]; then
	config_json="$config_json,
"
    fi
    config_json="${config_json}${config_entry}"
fi
make_config zeronet
if [ -n "$config_entry" ]; then
    if [ -n "$config_json" ]; then
	config_json="$config_json,
"
    fi
    config_json="${config_json}${config_entry}"
fi
echo "[
$config_json
]" > config.json

# build <dbname> <api_host> <api_port>
build () {
    DBNAME=$1
    DB=$DB_PREFIX$1
    API_HOST=$2
    API_PORT=$3

    if [ -z "$API_HOST" -o -z "$API_PORT" ]; then
	echo Skipping the build for $DB
    else
	
	cat > Makefile.database <<EOF
DATABASE=${DB}
DESTSUBDIR=tzscan
WITH_VERSION=true
API_HOST:=$API_HOST
API_PORT:=$API_PORT
NEED_PARSEXP=true
BASE64_3:=false
EOF

	./configure
	make

        # tzscan_openapi.json
	(cd www; ../_obuild/tzscan-openapi/tzscan-openapi.asm)

	mkdir out-$DB
	mv tzscan-api-server out-$DB/tzscan-api-server-$DB
	mv tzscan-crawler out-$DB/tzscan-crawler-$DB
	mv www out-$DB
	cp ../jsons/$DBNAME/api-config.json out-$DB/www
	cp config.json out-$DB
	sed -e "s/%DB%/$DB/g" ../scripts/run-tzscan-crawler.sh.temp > out-$DB/sample-run-tzscan-crawler-$DB.sh
	chmod +x out-$DB/sample-run-tzscan-crawler-$DB.sh
	sed -e "s/%DB%/$DB/g" -e "s/%API_PORT%/$API_PORT/g" ../scripts/run-tzscan-api-server.sh.temp > out-$DB/sample-run-tzscan-api-server-$DB.sh
	chmod +x out-$DB/sample-run-tzscan-api-server-$DB.sh
    fi
}


# AAA_host and AAA_port should be defined in build.config
build mainnet $mainnet_api_host $mainnet_api_port
build alphanet $alphanet_api_host $alphanet_api_port
build zeronet $zeronet_api_host $zeronet_api_port
