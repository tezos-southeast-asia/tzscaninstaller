This repository is a work-in-progress effort based on the official [tzscan gitlab repository](https://gitlab.com/tzscan/tzscan). While attempting to install it on our test servers, we ran into some issues and therefore have customized some areas of the code and sharing it here for everyone. Hopefully this allows others who are running the code to save some time customizing.

Many thanks to [Dailambda](https://gitlab.com/dailambda) for all their hard work in looking into package dependencies and assistance in guiding us to build OCaml code. There is also a repository of precise versions of the required packages and their source tarball hashes hosted at their repository.

- Environment: Ubuntu >= 18.04

## Build From the Script

### Configuration

Edit `build.config` to match with your environment, following the comments of fields in the file.

Note that some configuration settings are *hard-coded* into the server binaries.  If you change some configuration, you should better recompile everything.

### The build

```
$ ./build.sh
```

### Running services

#### Installation (or binary relocation)

We recommend keep the binaries at the place where generated, but you can move them anywhere with an appropriate setting: the environment variable `CAML_LD_LIBRARY_PATH` must contain following directories:

* `tzscaninstaller/_opam/lib/stublibs/`
* `tzscaninstaller/_opam/lib/ocaml/`

You can move files under these directories somewhere, too, as far as `CAML_LD_LIBRARY_PATH` points them correctly.

#### Tezos node

Tezos node must be running at `xxxnet_tezos_node` and well-sync'ed.

#### Crawler

```
$ ./tzscan-crawler --count --count-level 0 -1 config.json
```

#### API server

On `xxxnet_api_host`,

```
$ ./tzscan-api-server -p <xxxnet_api_port> --node-config config.json --api-config www/api-config.json
```

#### Web server

`www` contains the contents of the web server including JavaScript code.  You can expose these contents as `xxxnet_web_address` using any web server.
