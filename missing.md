# What is missing in OSS Tzscan?

Disclaimers

* Some of these missing elements may be due to our misconfiguration of servers.
* Data correctness is not checked at all, unless explicitly stated.

## Visually missing

Disclaimers

* Small visual differences are not listed.

### Home

http://node1.nyc.tezos.org.sg:8000/

* Marketcap info: empty. Priority: medium.
  Visually important, but it is just a copy of CoinMarketCap.
  There is a table `marketcap` and its reader code exists, but there is no writer.
* Staking Ratio: empty.
  The data should be obtaind from column `staking_balbances` of table `alpha.context_totals`,
  but there is no writer for `alpha.context_totals`.
* Roll owners: empty.
  The data should be obtained from tables `alpha.snapshot_owner` and `alpha.snapshot_rolls`,
  but there is no writer for them.

Number of Transactions, Originations, etc are largely different from tzscan.io.

### Blocks

* All tables in Baking rights (http://node1.nyc.tezos.org.sg:8000/baking-rights?r=10)
* Snapshots (http://node1.nyc.tezos.org.sg:8000/snapshot-blocks)

### Operations

* Nonces: Indicators lack the color.
  The numbers are displayed, therefore it should be a problem of CSS level.  Class `bg-green` and `bg-red` should be added.
   (http://node1.nyc.tezos.org.sg:8000/nonces?r=10)
   
### Accounts

Everything is displayed.

### Protocols

Priority: low.  We have some alternatives already?  Link?

* Voting periods is largely broken. (http://node1.nyc.tezos.org.sg:8000/proposals)
* All proposals: empty. (http://node1.nyc.tezos.org.sg:8000/all-proposals)

### Stats

* Roll distribution is empty (http://node1.nyc.tezos.org.sg:8000/rolls-distribution)
* Key numbers: empty (http://node1.nyc.tezos.org.sg:8000/context)
* Network: empty (http://node1.nyc.tezos.org.sg:8000/context)

### Charts

* Bakers: displayed but differently.  Data look OK, but the OSS version code should be old.  Priority: low. (http://node1.nyc.tezos.org.sg:8000/charts_bakers)
* Market prices: empty.  Priority: low.  (http://node1.nyc.tezos.org.sg:8000/market_prices)
* More charts: the section does not exist.  In tzscan.io, the url is https://tzscan.io/charts .  Priority: low.

### Misc

* FAQ: 404.  We can simply put a file there.  Priority: Low and easy.  (http://node1.nyc.tezos.org.sg:8000/faq)
* About us:  Definitely need to be changed.  Priority: High but easy. (http://node1.nyc.tezos.org.sg:8000/faq)
* API: 404 of a data file.  This seem to be generatable using `_obuild/tzscan-openapi/tzscan-openapi.asm`.  Priority: Medium and easy.  (http://node1.nyc.tezos.org.sg:8000/api)
* Download: APK file does not exist.  Priority: None.  (http://node1.nyc.tezos.org.sg:8000/tzscan.apk)

### Gear icon

* Dark theme is missing.  Priority: None.

## Alphanet and Zeronet

Working but seems with the same set of missing pieces.
